package com.octopusinvitro.auction_sniper;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FizzBuzzTest {
    @Test
    public void FizzBuzzNormalNumbers() {
        FizzBuzz fb = new FizzBuzz();
        Assertions.assertEquals("1", fb.convert(1));
        Assertions.assertEquals("2", fb.convert(2));
    }

    @Test
    public void FizzBuzzThreeNumbers() {
        FizzBuzz fb = new FizzBuzz();
        Assertions.assertEquals("Fizz", fb.convert(3));
    }

    @Test
    public void FizzBuzzFiveNumbers() {
        FizzBuzz fb = new FizzBuzz();
        Assertions.assertEquals("Buzz", fb.convert(5));
    }

    @Test
    public void FizzBuzzThreeAndFiveNumbers() {
        FizzBuzz fb = new FizzBuzz();
        Assertions.assertEquals("FizzBuzz", fb.convert(15));
    }
}