[![build status](https://gitlab.com/octopusinvitro/auction-sniper/badges/main/pipeline.svg)](https://gitlab.com/octopusinvitro/auction-sniper/commits/main)
[![Coverage Status](https://gitlab.com/octopusinvitro/auction-sniper/badges/main/coverage.svg)](https://gitlab.com/octopusinvitro/auction-sniper/badges/main/coverage.svg)


# README

Following the example project in "[Growing Object Oriented Software Guided by Tests](https://www.oreilly.com/library/view/growing-object-oriented-software/9780321574442/)". I read the book ages ago but I don't remember if I was following with the code 🤔.


## Setup

This project follows [Java's package naming conventions](https://docs.oracle.com/javase/tutorial/java/package/namingpkgs.html) (main package is `com.octopusinvitro.auction_sniper`). It uses [gradle](https://gradle.org/) as a build tool. The scripts inside the `bin` folder are convenient for common tasks. Make sure they are executable with:

```bash
chmod -R +x bin/*
```

## Testing

To run all tests:

```bash
bin/test
```

To filter tests:

```bash
bin/test TEST_CLASS_NAME
bin/test TEST_CLASS_NAME.TEST_METHOD_NAME
```

To see the test report:

```bash
firefox build/reports/tests/test/index.html
```